import { NgModule } from '@angular/core';

import { FlexLayoutModule } from "@angular/flex-layout";

import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatTableModule } from "@angular/material/table";
import { MatDialogModule } from "@angular/material/dialog";


@NgModule({
    declarations: [],
    exports: [
        FlexLayoutModule,
        MatToolbarModule,
        MatButtonModule,
        MatSelectModule,
        MatInputModule,
        MatFormFieldModule,
        MatTableModule,
        MatDialogModule
    ]
})
export class AppMaterialModule { }
