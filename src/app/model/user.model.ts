export interface User {
    id: number
    login: string
    name: string
    roles: string[]
}