import { NgModule } from '@angular/core';
import { NavigationStart, Router, RouterModule, Routes } from '@angular/router';
import { SessionService } from './service/session.service';
import { HomeAdminComponent } from './ui/admin/home/home.component';
import { HomeClientComponent } from './ui/client/home/home.component';
import { ListingsComponent } from './ui/client/listings/listings.component';
import { MyBookingsComponent } from './ui/client/my-bookings/my-bookings.component';
import { MyListingsComponent } from './ui/client/my-listings/my-listings.component';
import { LoginComponent } from './ui/login/login.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'admin', component: HomeAdminComponent },
    {
        path: 'client', component: HomeClientComponent, children: [
            { path: 'listings', component: ListingsComponent },
            { path: 'my-listings', component: MyListingsComponent },
            { path: 'my-bookings', component: MyBookingsComponent },
            { path: '', pathMatch: 'full', redirectTo: 'listings' }
        ]
    },
    { path: '', pathMatch: 'full', redirectTo: '/login' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

    constructor(private router: Router,
        private sessionService: SessionService) {
        this.setupRouterListener()
    }

    private setupRouterListener() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (!this.sessionService.isLoggedIn && event.url !== "/login") {
                    this.router.navigateByUrl('/login')
                } else if (this.sessionService.isLoggedIn) {

                    if ((!this.sessionService.isAdmin && event.url.startsWith('/admin')) ||
                        (!this.sessionService.isClient && event.url.startsWith('/client')) ||
                        event.url === '/login' || event.url === '/') {
                        this.goToHome()
                    }
                }
            }
        })
    }

    private goToHome() {
        if (this.sessionService.isAdmin) {
            this.router.navigateByUrl('/admin')
        }

        return this.router.navigateByUrl('/client')
    }

}
